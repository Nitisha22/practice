package linearSearch;

//https://leetcode.com/problems/richest-customer-wealth/
public class MaximunWealth {

	public static void main(String args[]) {
		int[][] accounts = { 
				{ 1, 2, 3 },
				{ 2, 4, 5 }
				};
		System.out.println(maxWealth(accounts));
	}

	static int maxWealth(int[][] accounts) {
		// TODO Auto-generated method stub
		int ans = Integer.MIN_VALUE;

		for (int[] person : accounts) {
			int sum = 0;
			for (int account : person) {
				sum += account;
			}
			if (sum > ans)
				ans = sum;
		}

		return ans;
	}
}
