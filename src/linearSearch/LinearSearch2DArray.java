package linearSearch;

import java.util.Arrays;

public class LinearSearch2DArray {

	public static void main(String args[]) {

		int[][] arr = { 
				{ 12, 34, 56 },
				{ 11, 23, 78, 99 },
				{ 14, 67, 54, 34, 90 } 
				};

		int target = 90;
		int[] res = search(arr, target);

		System.out.println(Arrays.toString(res));
		System.out.println("Max Element in 2D Array: " + max(arr));
		System.out.println("Min Element in 2D Array: " + min(arr));

	}

	// search in 2D Array
	static int[] search(int[][] arr, int target) {

		for (int row = 0; row < arr.length; row++) {
			System.out.println(arr[row].length);
			for (int col = 0; col < arr[row].length; col++) {
				if (arr[row][col] == target)
					return new int[] { row, col };
			}
		}

		return new int[] { -1, -1 };
	}

	// Max value in 2D Array
	static int max(int[][] arr) {
		int max = Integer.MIN_VALUE;

		for (int[] ints : arr) {
			for (int element : ints) {
				if (element > max)
					max = element;
			}
		}

		return max;
	}

	// Min in 2D Array
	static int min(int[][] arr) {
		int min = Integer.MAX_VALUE;

		for (int[] ints : arr) {
			for (int element : ints) {
				if (element < min)
					min = element;
			}
		}

		return min;

	}
}
