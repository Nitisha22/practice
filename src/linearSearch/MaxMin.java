package linearSearch;

public class MaxMin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] arr = new int[] { -2, 1, -4, 5, 3 };
		System.out.println(solve(arr));

	}

	public static int solve(int[] A) {

		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;

		for (int element : A) {
			if (element > max)
				max = element;
			if (element < min)
				min = element;
		}
		return max + min;

	}

}
