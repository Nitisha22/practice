package linearSearch;

public class linearSearch {

	public static void main(String args[]) {

		int[] arr = { 1, 3, 5, 2, 7 };
		int target = 5;

		System.out.println("Element is : " + search(arr, target));
		System.out.println("Index is : " + searchIndex(arr, target));

	}

	// search the element
	static int search(int[] arr, int target) {

		if (arr.length == 0)
			return -1;

		for (int element : arr) {
			if (element == target)
				return element;
		}

		return -1;
	}

	// search the element
	static int searchIndex(int[] arr, int target) {

		if (arr.length == 0)
			return -1;

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == target)
				return i;
		}

		return -1;
	}
}
