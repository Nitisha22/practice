package linearSearch;

public class EvenNoOfDigitsLC {

	public static void main(String args[]) {
		int[] arr = { 1, 23, 56, 781, 34567 };

		System.out.println("Even Digits in Array : " + findNumbers(arr));
		System.out.println("No. of Digits - Optimized Way :" + digits2(3456789));

	}

	static int findNumbers(int[] arr) {
		// TODO Auto-generated method stub
		int count = 0;
		for (int element : arr) {
			if ((even(element)))
				count++;
		}

		return count;
	}

	static boolean even(int num) {
		return (digits(num)) % 2 == 0;
	}

	static int digits(int num) {

		if (num < 0)
			num = num * -1;

		if (num == 0)
			return 1;

		int count = 0;

		while (num > 0) {
			count++;
			num = num / 10;
		}

		return count;
	}

	// Find digits in Optimised way
	static int digits2(int num) {
		return (int) (Math.log10(num) + 1);
	}

}
