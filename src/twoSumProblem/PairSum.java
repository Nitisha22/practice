package twoSumProblem;

import java.util.HashSet;

class PairSum {
	static boolean printpairs(int arr[], int sum) {
		HashSet<Integer> s = new HashSet<Integer>();
		for (int i = 0; i < arr.length; i++) {
			int temp = sum - arr[i];
			System.out.println("Temp : " + temp);
			// checking for condition
			if (s.contains(temp)) {
				System.out.println("Pair with given sum " + sum + " is (" + i + ", " + temp + ")");
				return true;
			}
			s.add(arr[i]);
		}
		return false;
	}

	// Driver Code
	public static void main(String[] args) {
		int A[] = { 1, 4, 45, 6, 10, 8 };
		int n = 12;
		printpairs(A, n);
	}
	

}
