package twoSumProblem;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSumHashMap {

	public static void main(String args[]) {

		int arr[] = { 1, 4, 45, 6, 10, -8 };
		int n = 16;
		int len = arr.length;

		int[] result = twoSum(arr, n);
		System.out.println(Arrays.toString(result));

	}

//	Leetcode solution
	public static int[] twoSum(int[] arr, int target) {

		Map<Integer, Integer> hashMap = new HashMap<>();
		for (int i = 0; i < arr.length; i++) {

			int temp = target - arr[i];

			if (hashMap.containsKey(temp)) {
				System.out.println(hashMap.get(temp) + "  -- temp : "+ temp+ "  --i : "+ i);
				return new int[] { hashMap.get(temp), i };
			} else
				hashMap.put(arr[i], i);

		}
		return new int[] {};

	}
}
