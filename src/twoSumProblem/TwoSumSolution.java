package twoSumProblem;

public class TwoSumSolution {

	public static boolean evaluateTwoSum(int array[], int sum, int length) {

		for (int i = 0; i < length - 1; i++) {
			for (int j = i + 1; j < length; j++) {
				if (array[i] + array[j] == sum) {
					System.out.println(array[i] + " " + array[j] + "\n" + "position are : " + i + " & " + j);
					return true;
				}
			}
		}

		return false;
	}

	public static void main(String args[]) {

		int arr[] = { 1, 4, 45, 6, 10, -8 };
		int n = 16;
		int len = arr.length;

		if(evaluateTwoSum(arr, n, len)) 
			System.out.println("Valid pair exists");
		else
			System.out.println("Valid pairs Not found !!");
		
	}

}
