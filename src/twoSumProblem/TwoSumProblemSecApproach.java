package twoSumProblem;

import java.util.Arrays;

public class TwoSumProblemSecApproach {

	public static void main(String args[]) {

		int arr[] = { -8, -5, 1, 2, 4, 5, 6, 14 };
		int len = arr.length;
		int n = 9;

		if (evaluateTwoSum(arr, n, len))
			System.out.println("Valid pair exists");
		else
			System.out.println("Valid pairs Not found !!");
	}

	private static boolean evaluateTwoSum(int[] arr, int sum, int len) {
		// TODO Auto-generated method stub

		Arrays.sort(arr);
		int l = 0;
		int r = len - 1;

		while (l < r) {
			if (arr[l] + arr[r] == sum) {
				System.out.println("Pairs are" + arr[l] + " & " + arr[r]);
				return true;
			} else if (arr[l] + arr[r] < sum)
				l++;
			else
				r--;
		}
		return false;
	}

}
