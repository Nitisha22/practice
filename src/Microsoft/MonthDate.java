package linearSearch;

public class MonthDate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(solution("?1-31"));
		System.out.println(solution("02-??"));
		System.out.println(solution("??-4?"));
		System.out.println(solution("09-31"));


	}

	public static boolean valid(int month, int date, String s) {
		String mon = String.valueOf(month);
		String dat = String.valueOf(date);
		if (month < 10)
			mon = "0" + mon;
		if (date < 10)
			dat = "0" + dat;
		String temp = s.substring(0, 2);
		temp += s.substring(3);
		String temp2 = mon + dat;
		for (int i = 0; i < 4; i++) {
			if (temp.charAt(i) == '?')
				continue;
			if (temp.charAt(i) != temp2.charAt(i))
				return false;
		}
		return true;
	}

	public static String form(int month, int date) {
		String mon = String.valueOf(month);
		String dat = String.valueOf(date);
		if (month < 10)
			mon = "0" + mon;
		if (date < 10)
			dat = "0" + dat;
		return mon + "-" + dat;
	}

	public static String solution(String s) {
		// Implement your solution here
		String[] monthDateArr = s.split("-");
		String ans = "";
		String wrong = "xx-xx";

		for (int month = 12; month >= 1; month--) {
			int date = 31;
			if (month == 2)
				date = 28;
			else if (month < 8 && month % 2 == 1)
				date = 31;
			else if (month >= 8 && month % 2 == 0)
				date = 31;
			else if (month < 8 && month % 2 == 0)
				date = 30;
			else if (month >= 8 && month % 2 == 1)
				date = 30;
			// System.out.print(date + " " + date);
			for (; date >= 1; date--) {
				if (valid(month, date, s)) {
					return form(month, date);
				}
			}
		}
		return wrong;

	}

}
