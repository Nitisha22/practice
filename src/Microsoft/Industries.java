package linearSearch;

import java.util.Arrays;

public class Industries {

	public static int sol(int[] temp) {
		if (null == temp)
			return 0;
		int len = temp.length;
		if (len < 1)
			return 0;
		if (len == 1)
			return temp[0] < 0 ? -1 * temp[0] : 0;
		int[] A = new int[len + 3];
		Arrays.fill(A, 0);
		for (int i = 1; i < len + 1; i++) {
			A[i] = temp[i - 1];
		}
		int l = 1, r = len, ans = 0;
		while (l <= r) {
			// left
			int curPol = A[l] + A[l - 1] + A[l + 1];
			if (curPol < 0) {
				ans += -1 * curPol;
				if (A[l] < 0) {
					A[l] += -1 * curPol;
				} else if (A[l + 1] < 0) {
					A[l + 1] += -1 * curPol;
				}
			}
			if (l == r)
				break;
			// right
			curPol = A[r] + A[r - 1] + A[r + 1];
			if (curPol < 0) {
				ans += -1 * curPol;
				if (A[r] < 0) {
					A[r] += -1 * curPol;
				} else if (A[r - 1] < 0) {
					A[r - 1] += -1 * curPol;
				}
			}
			l++;
			r--;
		}
		return ans;
	}

	public static void main(String[] args) throws java.lang.Exception {
		int[] A = new int[] { -2, 1, -3, 1 };
		System.out.println(sol(A));
		A = new int[] { 2, -1, -1 };
		System.out.println(sol(A));
		A = new int[] { 2, -3, 1 };
		System.out.println(sol(A));
		A = new int[] { 1, -3, 2 };
		System.out.println(sol(A));
		A = new int[] { -3, 2, 4, -5, 3 };
		System.out.println(sol(A));
	}

}
