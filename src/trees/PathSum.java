package trees;

import java.util.Stack;

import trees.Diameter.TreeNode;

//https://leetcode.com/problems/path-sum/
public class PathSum {

	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode() {
		}

		TreeNode(int val) {
			this.val = val;
		}

		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}

	public boolean hasPathSum(TreeNode root, int targetSum) {

		if (root == null)
			return false;

		Stack<TreeNode> nodeStack = new Stack();
		Stack<Integer> sumStack = new Stack();

		nodeStack.push(root);
		sumStack.push(targetSum - root.val);

		while (!nodeStack.isEmpty()) {

			TreeNode currentNode = nodeStack.pop();
			int currentSum = sumStack.pop();
			if (currentNode.left == null && currentNode.right == null && currentSum == 0)
				return true;

			if (currentNode.left != null) {
				nodeStack.push(currentNode.left);
				sumStack.push(currentSum - currentNode.left.val);
			}

			if (currentNode.right != null) {
				nodeStack.push(currentNode.right);
				sumStack.push(currentSum - currentNode.right.val);
			}

		}
		return false;
	}

}
