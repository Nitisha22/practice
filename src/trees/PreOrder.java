package trees;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

//https://leetcode.com/problems/n-ary-tree-preorder-traversal/description/
public class PreOrder {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	class Node {
		public int val;
		public List<Node> children;

		public Node() {
		}

		public Node(int _val) {
			val = _val;
		}

		public Node(int _val, List<Node> _children) {
			val = _val;
			children = _children;
		}

	}	
	
	 public List<Integer> preorder(Node root) {
	        
	        List<Integer> ans = new ArrayList<Integer>();
			Stack<Node> stack = new Stack<>();

			if (root == null)
				return ans;

			stack.push(root);

			while (!stack.isEmpty()) {

				Node current = stack.pop();
				ans.add(current.val);

				for (int i = current.children.size() -1; i >=0 ; i--) {
	                Node child = current.children.get(i);
					stack.push(child);
				}

			}
			return ans;
	    }	

}
