package trees;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

//https://leetcode.com/problems/binary-tree-inorder-traversal/
public class InorderTraversal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public class TreeNode {
		     int val;
		      TreeNode left;
		      TreeNode right;
		      TreeNode() {}
		      TreeNode(int val) { this.val = val; }
		      TreeNode(int val, TreeNode left, TreeNode right) {
		          this.val = val;
		          this.left = left;
		          this.right = right;
		      }
		  }

	public List<Integer> inorderTraversal(TreeNode root) {

		List<Integer> ans = new ArrayList<>();
		Stack<TreeNode> stack = new Stack<>();

		if (root == null)
			return ans;

		TreeNode current = root;

		while (current != null || !stack.isEmpty()) {

			while (current != null) {
				stack.push(current);
				current = current.left;
			}

			current = stack.pop();
			ans.add(current.val);
			current = current.right;
		}

		return ans;
	}

}
