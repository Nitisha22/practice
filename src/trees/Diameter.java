package trees;

//https://leetcode.com/problems/diameter-of-binary-tree/
public class Diameter {

	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode() {
		}

		TreeNode(int val) {
			this.val = val;
		}

		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}

	static int diameter;

	public static int diameterOfBinaryTree(TreeNode root) {

		diameter = 0;

		getMaxDepth(root);

		return diameter;

	}

	public static int getMaxDepth(TreeNode root) {

		if (root == null) {
			return 0;
		}
		int leftSubTreeDepth = getMaxDepth(root.left);
		int rightSubTreeDepth = getMaxDepth(root.right);

		diameter = Math.max(diameter, leftSubTreeDepth + rightSubTreeDepth);

		return Math.max(leftSubTreeDepth, rightSubTreeDepth) + 1;

	}
}
