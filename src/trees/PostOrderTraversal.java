package trees;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

//https://leetcode.com/problems/n-ary-tree-postorder-traversal/
public class PostOrderTraversal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	class Node {
		public int val;
		public List<Node> children;

		public Node() {
		}

		public Node(int _val) {
			val = _val;
		}

		public Node(int _val, List<Node> _children) {
			val = _val;
			children = _children;
		}

	}

	public List<Integer> postorder(Node root) {

		LinkedList<Integer> ans = new LinkedList<Integer>();
		Stack<Node> stack = new Stack<>();

		if (root == null)
			return ans;

		stack.push(root);

		while (!stack.isEmpty()) {

			Node current = stack.pop();
			ans.addFirst(current.val);

			for (Node child : current.children) {
				stack.push(child);
			}

		}
		return ans;
	}

}
