package recursion;

public class Subsequence {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		subsequence("", "abc");
	}

	public static void subsequence(String p, String up) {

		if (up.isEmpty()) {
			System.out.println(p);
			return;
		}

		char ch = up.charAt(0);
		System.out.println("Substring: "+up.substring(1));
		subsequence(p + ch, up.substring(1));
		subsequence(p, up.substring(1));
	}

}
