package recursion;

public class PrintNum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int num = 5;
		printRevNumber(num);

	}

	public static void printNumber(int n) {

		if (n == 0)
			return;

		System.out.println(n);
		printNumber(n - 1);
	}

	public static void printRevNumber(int n) {

		if (n == 0)
			return;

		printRevNumber(n - 1);
		System.out.println(n);

	}

}
