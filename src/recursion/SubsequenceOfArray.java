package recursion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SubsequenceOfArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] arr = { 1, 2, 2, 2, 3 };
		List<List<Integer>> ans = subsequence(arr);
		for (List<Integer> list : ans)
			System.out.println(list);

	}

	public static List<List<Integer>> subsequence(int[] arr) {

		List<List<Integer>> outerList = new ArrayList<>();

		outerList.add(new ArrayList<>());

		for (int num : arr) {

			int n = outerList.size();

			for (int i = 0; i < n; i++) {
				List<Integer> innerList = new ArrayList<>(outerList.get(i));
				innerList.add(num);
//				outerList.add(innerList);
				if(!outerList.contains(innerList))
					outerList.add(innerList);
//				System.out.println("Inner---\n");
//				for (int l : innerList)
//					System.out.println(l);
//
//				System.out.println("Outer---\n");
//
//				for (List<Integer> ol : outerList)
//					System.out.println(ol);
//				System.out.println("\n");

			}
		}
		return outerList;

	}

}
