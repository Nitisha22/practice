package recursion;

import java.util.ArrayList;

public class Permutations {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		permutations("", "abc");
		System.out.println("-------------------------------");
		permutationsArray("", "abc");
	}

	public static void permutations(String p, String up) {

		if (up.isEmpty()) {
			System.out.println(p);
			return;
		}

		char ch = up.charAt(0);

		for (int i = 0; i <= p.length(); i++) {

			String f = p.substring(0, i);
			String l = p.substring(i, p.length());

			System.out.println("first : " + f + "\nlast : " + l);
			System.out.println("f+ch+l : " + f + ch + l);

			permutations(f + ch + l, up.substring(1));
		}
	}

	public static ArrayList<String> permutationsArray(String p, String up) {

		if (up.isEmpty()) {
			ArrayList<String> list = new ArrayList<>();
			System.out.println(p);
			list.add(p);
			return list;
		}

		char ch = up.charAt(0);

		ArrayList<String> ans = new ArrayList<>();

		for (int i = 0; i <= p.length(); i++) {

			String f = p.substring(0, i);
			String l = p.substring(i, p.length());
			ans.addAll(permutationsArray(f + ch + l, up.substring(1)));
		}
		return ans;
	}

}
