package recursion;

public class RotatedBinarySearch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = { 1, 2, 3, 6, 7, 9, 8 };
	}

	public static int binarySearch(int[] arr, int s, int e, int target) {

		if (s < e)
			return -1;

		int m = s + (s - e) / 2;
		if (m == target)
			return m;

		if (arr[s] <= arr[m]) {
			if (target >= arr[s] && target <= arr[m]) {
				return binarySearch(arr, s, m - 1, target);
			} else
				return binarySearch(arr, m + 1, e, target);
		}

		if (target >= arr[m] && target <= arr[e])
			return binarySearch(arr, m + 1, e, target);

		return binarySearch(arr, s, e - 1, target);

	}
}
