package recursion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Dice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		dice("", 4);
		System.out.println(diceAll("", 5));
	}

	public static void dice(String p, int target) {

		if (target == 0) {
			System.out.println(p);
			return;
		}

		System.out.println(" p :" + p + "\n target :" + target);

		for (int i = 1; i <= 6 && i <= target; i++) {
			dice(p + i, target - i);
		}
	}

	public static ArrayList<String> diceAll(String p, int target) {

		if (target == 0) {
			ArrayList<String> list = new ArrayList<>();
			list.add(p);
			return list;
		}

		ArrayList<String> ans = new ArrayList<>();
		for (int i = 1; i <= 6 && i <= target; i++) {
			ans.addAll(diceAll(p + i, target - i));
		}

		return ans;
	}

}
