package recursion;

public class Product {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n = 1342;
		System.out.println(digitsproduct(n));
	}

	public static int digitsproduct(int n) {

		if (n % 10 == n)
			return n;

		return digitsproduct(n / 10) * (n % 10);
	}
}
