package recursion;

public class CountZeros {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int n = 3020400;

		System.out.println(count(n));
	}

	static int count(int n) {
		return helper(n, 0);
	}

	private static int helper(int n, int i) {
		// TODO Auto-generated method stub
		if (n == 0)
			return i;

		int rem = n % 10;
		if (rem == 0)
			return helper(n / 10, i + 1);

		return helper(n / 10, i);

	}

}
