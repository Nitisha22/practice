package recursion;

public class DigitSum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int n = 1342;
		System.out.println(digitsum(n));
	}

	public static int digitsum(int n) {

		if (n == 0)
			return 0;

		return digitsum(n / 10) + n % 10;
	}

}
