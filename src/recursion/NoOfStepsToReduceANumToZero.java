package recursion;

public class NoOfStepsToReduceANumToZero {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 35;
		System.out.println(numberOfSteps(num));
	}

	public static int numberOfSteps(int num) {
		return count(num, 0);
	}

	public static int count(int num, int count) {

		if (num == 0)
			return count;

		if (num % 2 == 0)
			return count(num / 2, ++count);

		return count(num - 1, ++count);
	}

}
