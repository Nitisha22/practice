package recursion;

public class sortedArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] arr = { 1, 2, 3, 7, 5 };
		System.out.println(sortedArray(arr, 0));

	}

	public static boolean sortedArray(int[] arr, int index) {

		if (index == arr.length - 1)
			return true;

		return arr[index] < arr[index + 1] && sortedArray(arr, index + 1);
	}
}
