package EasyLevel;

import java.util.HashMap;
import java.util.Map;

//https://leetcode.com/problems/find-the-difference/description/
public class FindDifference {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static char findTheDifference(String s, String t) {

		Map<Character, Integer> map = new HashMap<>();

		for (char ch : s.toCharArray()) {
			if (map.containsKey(ch))
				map.put(ch, map.get(ch) + 1);
			else
				map.put(ch, 1);
		}

		for (char charT : t.toCharArray()) {
			if (!map.containsKey(charT) || map.get(charT) == 0)
				return charT;
			else
				map.put(charT, map.get(charT) - 1);
		}

		return Character.MIN_VALUE;
	}

}
