package EasyLevel;

import java.util.Arrays;

//https://leetcode.com/problems/plus-one/
public class PlusOne {

	public static void main(String args[]) {

		int[] digits = new int[] { 1, 2, 3 };
		int[] digitsCheck = new int[] { 9, 9, 9 };
		System.out.println(Arrays.toString(plusOne(digits)));
		System.out.println(Arrays.toString(plusOne(digitsCheck)));

	}

	public static int[] plusOne(int[] digits) {

		int i = digits.length - 1;

		while (i >= 0) {
			if (digits[i] != 9) {
				digits[i] = digits[i] + 1;
				return digits;
			}

			digits[i] = 0;
			i--;
		}

		int[] res = new int[digits.length + 1];
		res[0] = 1;

		return res;

	}

}
