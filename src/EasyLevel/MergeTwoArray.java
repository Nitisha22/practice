package EasyLevel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MergeTwoArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] arr1 = { 1, 2, 2, 3, 4, 5, 5 };
		int[] arr2 = { 4, 6, 7, 9 };

		IntStream stream = Arrays.stream(arr1);

		System.out.println(stream.max().getAsInt());
		System.out.println("\n");

		Arrays.stream(merge(arr1, arr2)).forEach(n -> System.out.println(n));
	}

	public static int[] merge(int[] arr1, int[] arr2) {

		int[] result = new int[arr1.length + arr2.length];

		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < arr1.length; i++) {

			if (map.containsKey(arr1[i])) {

				map.put(arr1[i], map.get(arr1[i]) + 1);
			} else
				map.put(arr1[i], 1);
		}

		for (int i = 0; i < arr2.length; i++) {

			if (map.containsKey(arr2[i])) {

				map.put(arr2[i], map.get(arr2[i]) + 1);
			} else
				map.put(arr2[i], 1);
		}

		int count = 0;
		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {

			for (int i = 0; i < entry.getValue(); i++) {
				result[count] = entry.getKey();
				count++;
			}

		}

		return result;

	}

}
