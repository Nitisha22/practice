package EasyLevel;

//https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
public class BestTimeToBuyAndSell {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static int maxProfit(int[] prices) {

		int min = Integer.MAX_VALUE;
		int max_profit = 0;

		for (int i = 0; i < prices.length; i++) {

			if (prices[i] < min)
				min = prices[i];
			else if (prices[i] - min > max_profit) {
				max_profit = prices[i] - min;
			}

		}

		return max_profit;
	}
	
//	https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/
	 public static int maxProfitII(int[] prices) {

	        int max_profit = 0;

	        for(int i = 0 ; i < prices.length - 1; i++){

	            if(prices[i + 1] > prices [i])
	                max_profit += prices[i + 1] - prices [i];

	        }

	        return max_profit;
	        
	    }

}
