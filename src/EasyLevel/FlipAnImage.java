package EasyLevel;

import java.util.Arrays;

public class FlipAnImage {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[][] image = { { 1, 1, 0 }, { 1, 0, 1 }, { 0, 0, 0 } };
		System.out.println(Arrays.deepToString(flipAndInvert(image)));
	}

	public static int[][] flipAndInvert(int[][] image) {

		for (int i = 0; i < image.length; i++) {

			int low = 0;
			int high = image[0].length - 1;

			while (low <= high) {

				if (image[i][low] == image[i][high]) {

					image[i][low] = image[i][low] ^ 1;
					image[i][high] = image[i][low];

				}

				low++;
				high--;

			}

		}
		return image;

	}

}
