package EasyLevel;

public class MagicNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int n = 6;
		int ans = 0;
		int base = 5;

		while (n > 0) {

			int last = n & 1;
			n = n >> 1;
			ans += last * base;
			base = base * 5;

		}

		System.out.println(ans);
	}
	
//	 6 -->  
//	1 1 0  & 1 = 0 0 0
//	1 1 & 1 = 0 1
//	1 & 1 = 1
	
//	0 * 5 ^ 1 + 1 * 5 ^2 + 1 * 5^ 3
	

}
