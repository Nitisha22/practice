package EasyLevel;

import java.util.HashMap;
import java.util.Map;

//https://leetcode.com/problems/single-number/
public class SingleNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public int singleNumber(int[] nums) {

		int result = 0;

		HashMap<Integer, Integer> map = new HashMap<>();
		for (int num : nums) {

			if (map.containsKey(num))
				map.put(num, map.get(num) + 1);
			else
				map.put(num, 1);
		}

		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			if (entry.getValue() == 1) {
				result = entry.getKey();
				break;
			}
		}

		return result;
	}

}
