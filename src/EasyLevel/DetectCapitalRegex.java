package EasyLevel;

//https://leetcode.com/problems/detect-capital/
public class DetectCapitalRegex {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(detectCapitalUse("Flag"));
	}

	public static boolean detectCapitalUse(String word) {
		return word.matches("[A-Z]*|[A-Z][a-z]+|[a-z]+");
	}
}
