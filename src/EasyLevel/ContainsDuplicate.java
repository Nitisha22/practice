package EasyLevel;

import java.util.HashMap;
import java.util.HashSet;

//https://leetcode.com/problems/contains-duplicate/
public class ContainsDuplicate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public boolean containsDuplicate(int[] nums) {

		HashSet<Integer> numbers = new HashSet<>();
		for (int num : nums) {
			if (numbers.contains(num))
				return true;

			numbers.add(num);

		}
		return false;

	}
	
//	https://leetcode.com/problems/contains-duplicate-ii/
	 public boolean containsNearbyDuplicate(int[] nums, int k) {

	        HashMap<Integer,Integer> set = new HashMap<>();

	        for(int i = 0; i< nums.length; i++){

	            if(set.containsKey(nums[i])) {
	               if(Math.abs(set.get(nums[i]) - i) <= k)
	                     return true;
	                }

	            set.put(nums[i], i);

	        }

	        return false;
	        
	    }

}
