package java8;

import java.util.ArrayList;

interface FunctionalInterface {

	void printNum(int x);

}

interface CalculateFunction {

	int calculate(int n, int m);

}

public class LambdaExpression {

	public static void main(String args[]) {

		FunctionalInterface functionalInterface = (int x) -> {
			System.out.println(2 * x);
		};

		functionalInterface.printNum(5);

		ArrayList<Integer> arrayList = new ArrayList<>();
		arrayList.add(5);
		arrayList.add(7);
		arrayList.add(9);
		arrayList.add(11);

		arrayList.forEach(n -> System.out.println(n));

		CalculateFunction calculateAddFunction = (int n, int m) -> {
			System.out.println("\n Inside calculateAddFunction");
			System.out.println(n + m);
			return n + m;
		};

		calculateAddFunction.calculate(5, 4);

		CalculateFunction calculateMultiplyFunction = (int n, int m) -> {
			System.out.println("\n Inside calculateMultiplyFunction");
			System.out.println(n * m);
			return n * m;
		};

		calculateMultiplyFunction.calculate(5, 4);

		LambdaExpression lambdaExpression = new LambdaExpression();
		lambdaExpression.operate(1, 2, calculateAddFunction);
		lambdaExpression.operate(1, 2, calculateMultiplyFunction);

	}

	public int operate(int a, int b, CalculateFunction calculateFunction) {

		return calculateFunction.calculate(a, b);
	}
}
