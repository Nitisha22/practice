package slidingWindow;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

//https://www.youtube.com/watch?v=uUXXEgK2Jh8&list=PL_z_8CaSLPWeM8BDJmIYDaoQ5zuwyxnfj&index=4&t=834s
public class FirstNegativeNumberInSubarray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nums = { 15, -2, 3, 4, 5, -6, 7 };

		System.out.println(findNegative(nums, 3));

	}

	public static ArrayList<Integer> findNegative(int[] nums, int k) {

		int i = 0;
		int j = 0;

		Queue<Integer> queue = new LinkedList<>();

		ArrayList<Integer> resultArr = new ArrayList<>();

		while (j < nums.length) {

			if (nums[j] < 0)
				queue.add(nums[j]);

			if (j - i + 1 == k) {

				if (queue.isEmpty())
					resultArr.add(0);
				else {
					resultArr.add(queue.peek());
					if (nums[i] == queue.peek())
						queue.remove();
				}

				i++;
				j++;

			} else
				j++;

		}

		return resultArr;

	}

}
