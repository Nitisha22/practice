package slidingWindow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FindAnagrams {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println(findAnagrams("cbaebabacd", "abc"));

	}

	public static List<Integer> findAnagrams(String s, String p) {

		List<Integer> result = new ArrayList<>();

		HashMap<Character, Integer> map = new HashMap<>();

		int i = 0;
		int j = 0;

		int k = p.length();

		// Adding all character to map along with its count
		for (Character ch : p.toCharArray()) {
			if (map.containsKey(ch)) {
				map.put(ch, map.get(ch) + 1);
			} else {
				map.put(ch, 1);
			}
		}

		int count = map.size();


		while (j < s.length()) {

			char ch = s.charAt(j);

			if (map.containsKey(ch)) {
				map.put(ch, map.get(ch) - 1);
				if (map.get(ch) == 0) {
					count--;
				}
			}

			if (j - i + 1 < k) {

				j++;

			} else if (j - i + 1 == k) {

				if (count == 0) {
					result.add(i);
				}

				char ch1 = s.charAt(i);
				if (map.containsKey(ch1)) {
					map.put(ch1, map.get(ch1) + 1);

					if (map.get(ch1) == 1) {
						count++;
					}
				}

				i++;
				j++;

			}
		}
		return result;
	}

}
