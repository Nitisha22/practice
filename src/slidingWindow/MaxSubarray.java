package slidingWindow;

//https://www.youtube.com/watch?v=KtpqeN0Goro&list=PL_z_8CaSLPWeM8BDJmIYDaoQ5zuwyxnfj&index=3
public class MaxSubarray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] nums = { 15, 2, 3, 4, 5, 6, 7 };

		System.out.println(maxSubArray(nums, 3));

	}

	public static int maxSubArray(int[] nums, int k) {

		int length = nums.length;
		int i = 0;
		int j = 0;
		int sum = 0;

		int maxSum = Integer.MIN_VALUE;

		while (j < length) {

			sum = sum + nums[j];

			if (j - i + 1 == k) {

				maxSum = Math.max(maxSum, sum);
				sum = sum - nums[i];
				i++;
				j++;

			} else
				j++;

		}

		System.out.println("Maximum sub array is - " + maxSum);
		return maxSum;

	}
}
