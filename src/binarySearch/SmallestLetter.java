package binarySearch;

// https://leetcode.com/problems/find-smallest-letter-greater-than-target/
public class SmallestLetter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		char[] letters = { 'b', 'c', 'e', 'g' };

		char target = 'g';

		System.out.println(findSmallestLetter(letters, target));

	}

	static char findSmallestLetter(char[] letters, char target) {

		int start = 0;
		int end = letters.length - 1;

		while (start <= end) {
			int mid = start + (end - start) / 2;

			if (target < letters[mid])
				end = mid - 1;
			else
				start = mid + 1;

		}
		// letters[start % letters.length] or if(start == length of array)
		if (start == letters.length)
			return letters[0];

		return letters[start];

	}
}
