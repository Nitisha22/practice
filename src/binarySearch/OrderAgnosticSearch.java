package binarySearch;

public class OrderAgnosticSearch {

	// OrderAgnosticSearch means in which sorting type is not defined, we have to find if its ascending or descending
	
	public static void main(String[] args) {

		int[] arr = { 99, 87, 78, 67, 54, 34, 1, 0 };
		int target = 78;
		System.out.println(orderAgnosticSearch(arr, target));
	}

	static int orderAgnosticSearch(int[] arr, int target) {
		// TODO Auto-generated method stub

		int start = 0;
		int end = arr.length - 1;
		boolean isAsc = arr[start] < arr[end];

		while (start <= end) {
			int mid = start + (end - start) / 2;

			if (target == arr[mid])
				return mid;

			if (isAsc) {
				if (target < arr[mid])
					end = mid - 1;
				else
					start = mid + 1;

			} else {
				if (target > arr[mid])
					end = mid - 1;
				else
					start = mid + 1;

			}
		}
		return -1;
	}

}
