package binarySearch;

import java.util.Arrays;

// Row Column Search when matrix is sorted row and col wise both
public class RowColSearch {

	public static void main(String[] args) {

		int[][] matrix = { 
				{ 1, 5, 10, 20 },
				{ 2, 7, 13, 25 }, 
				{ 3, 9, 14, 27 },
				{ 4, 11, 15, 30 }
				};

		int target = 9;
		System.out.println(Arrays.toString(search(matrix, target)));
	}
	// Here row = 0 and col = matrix length -1
	// if element is found at particular row and column the index is returned
	// if target is > then current element means it will be find in next row as it
	// checking 1D array every time means checking in a row
	// else if its less than current element than it must lie at col-- as in that
	// particlar col all the numbers will be greater than the target element

	static int[] search(int[][] matrix, int target) {

		int row = 0;
		int col = matrix.length - 1;

		while (row < matrix.length && col >= 0) {

			if (matrix[row][col] == target)
				return new int[] { row, col };

			if (matrix[row][col] < target)
				row++;
			else
				col--;

		}

		return new int[] { -1, -1 };
	}

}
