package binarySearch;

//https://www.youtube.com/watch?v=4WmTRFZilj8&list=PL_z_8CaSLPWeYfhtuKHj-9MpYb6XQJ_f2&index=7s
public class RotationCountBS {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nums = { 5, 9, 1, 2, 3, 4 };

		if (findPivot(nums) != -1)
			System.out.println(findPivot(nums));
		else
			System.out.println(-1);
	}

	private static int findPivot(int[] nums) {
		// TODO Auto-generated method stub

		int start = 0;
		int end = nums.length - 1;

		while (start <= end) {

			int mid = start + (end - start) / 2;
			int prev = (mid + nums.length - 1) % nums.length;
			int next = (mid + 1) % nums.length;

			System.out.println("start :" + start + "\n mid :" + mid + "\n" + "end :" + end + "\n --------------");

			if (nums[mid] <= nums[prev] && nums[mid] <= nums[next])
				return mid;
			if (nums[start] <= nums[mid])
				start = mid + 1;
			else
				end = mid - 1;

		}

		return -1;
	}

}
