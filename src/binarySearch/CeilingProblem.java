package binarySearch;

// smallest number greater than or equal to target --> Ceiling

public class CeilingProblem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] arr = { 1, 2, 4, 6, 7, 45, 67, 89 };

		int target = 5;

		System.out.println(findCeiling(arr, target));

	}

	static int findCeiling(int[] arr, int target) {

		int start = 0;
		int end = arr.length - 1;

		// if target is greater than end of array
		if (target > end)
			return -1;

		while (start <= end) {
			int mid = start + (end - start) / 2;

			if (target > arr[mid])
				start = mid + 1;
			else if (target < arr[mid])
				end = mid - 1;
			else
				return arr[mid];

		}
		return arr[start];

	}

}
