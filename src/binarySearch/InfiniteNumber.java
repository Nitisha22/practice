package binarySearch;

//https://www.geeksforgeeks.org/find-position-element-sorted-array-infinite-numbers/

//Infinite sorted Array is given, so we are not aware about the length of the array here

public class InfiniteNumber {

	public static void main(String args[]) {

		int[] arr = { 1, 3, 5, 5, 15, 18, 76, 89 };
		int target = 15;
		System.out.println(ans(arr, target));
	}

//	Here, we are taking small chunks of array, firstly --> start = 0, and end = 1, then
//	if check if target is > arr[end] then we will increase the size of chunk * 2
	static int ans(int[] arr, int target) {

		int start = 0;
		int end = 1;

		while (target > arr[end]) {
			
			int temp = end + 1;

			// end - start + 1 as end and start are indexes and we need to find size of
			// array so plus 1 in it
			end = end + (end - start + 1) * 2;
			start = temp;
		}

		return binarySearch(arr, target, start, end);

	}

	// Binar Search Code
	static int binarySearch(int[] arr, int target, int start, int end) {

		while (start <= end) {
			int mid = start + (end - start) / 2;

			if (target < arr[mid])
				end = mid - 1;
			else if (target > arr[mid])
				start = mid + 1;
			else
				return mid;

		}
		return -1;

	}
}
