package binarySearch;

import java.util.Arrays;

public class RowMajorOrder {

	public static void main(String args[]) {
		int[][] matrix = { 
							{ 1, 2, 3 }, 
							{ 4, 5, 6 }, 
							{ 7, 8, 9 } 
						};
		int target = 2;
		System.out.println(Arrays.toString(searchMatrix(matrix, target)));
		System.out.println(Arrays.toString(binarySearchMatrix(matrix, 7)));
	}

	static int[] searchMatrix(int[][] matrix, int target) {

		int row = 0;
		int col = matrix.length - 1;

		while (row < matrix.length) {
			if (matrix[row][col] == target)
				return new int[] { row, col };
			else if (target > matrix[row][col])
				row++;
			else
				return new int[] { row, binarySearch(matrix[row], target) };

		}

		return new int[] { -1, -1 };
	}

	static int[] binarySearchMatrix(int[][] matrix, int target) {

		int start = 0;
		int end = matrix.length - 1;

		while (start <= end) {
			int mid = start + (end - start) / 2;

			if (target >= matrix[mid][0] && target <= matrix[mid][end])
				return new int[] { mid, binarySearch(matrix[mid], target) };
			else if (target < matrix[mid][0])
				end = mid - 1;
			else
				start = mid + 1;

		}

		return new int[] { -1, -1 };
	}

	static int binarySearch(int[] arr, int target) {

		int start = 0;
		int end = arr.length - 1;

		while (start <= end) {
			int mid = start + (end - start) / 2;

			if (target < arr[mid])
				end = mid - 1;
			else if (target > arr[mid])
				start = mid + 1;
			else
				return mid;

		}
		return -1;

	}

}
