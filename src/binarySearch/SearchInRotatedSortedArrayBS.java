package binarySearch;

public class SearchInRotatedSortedArrayBS {

	public static void main(String[] args) {

		int[] arr = { 9, 2, 3, 4 };
		System.out.println(search(arr, 2));
	}

	public static int search(int[] arr, int target) {

		int pivot = findPivot(arr);
		System.out.println(pivot);
		int ans = 0;
		ans = binarySearch(arr, 0, pivot - 1, target);
		System.out.println(ans);
		if (ans != -1)
			return ans;
		return binarySearch(arr, pivot, arr.length - 1, target);
	}

	private static int findPivot(int[] nums) {
		// TODO Auto-generated method stub

		int start = 0;
		int end = nums.length - 1;

		while (start <= end) {

			int mid = start + (end - start) / 2;
			int prev = (mid + nums.length - 1) % nums.length;
			int next = (mid + 1) % nums.length;

			System.out.println("start :" + start + "\n mid :" + mid + "\n" + "end :" + end + "\n --------------");

			if (nums[mid] <= nums[prev] && nums[mid] <= nums[next])
				return mid;
			if (nums[start] <= nums[mid])
				start = mid + 1;
			else
				end = mid - 1;

		}

		return -1;
	}

	public static int binarySearch(int[] arr, int start, int end, int target) {

		while (start <= end) {
			int mid = start + (end - start) / 2;

			if (target < arr[mid])
				end = mid - 1;
			else if (target > arr[mid])
				start = mid + 1;
			else
				return mid;

		}

		return -1;
	}

}
