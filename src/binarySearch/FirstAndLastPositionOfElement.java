package binarySearch;

//https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/
import java.util.Arrays;

public class FirstAndLastPositionOfElement {

	public static void main(String[] args) {

		int[] nums = { 5, 7, 7, 7, 8, 8, 8, 10 };
		int target = 7;
		int[] ans = searchRange(nums, target);
		System.out.println("Range of given element is :" + Arrays.toString(ans));
	}

	// Initializing ans as -1, -1 if element is not found, then searching the first
	// occurrence and if its found then
	// then searching last occurrence of target value
	public static int[] searchRange(int[] nums, int target) {
		int[] ans = { -1, -1 };

		ans[0] = searchElementsRange(nums, target, true);

		if (ans[0] != -1)
			ans[1] = searchElementsRange(nums, target, false);

		System.out.println("Start : " + ans[0]);
		System.out.println("End : " + ans[1]);

		return ans;

	}

	// It takes arr, target and boolean whether it is first occurrence or not ,
	// then applying binary search and if element is found in the middle then for
	// finding first occr its end is changed and for finfing last occ its start is
	// changed
	public static int searchElementsRange(int[] nums, int target, boolean firstOccurence) {

		int ans = -1;
		int start = 0;
		int end = nums.length - 1;

		while (start <= end) {

			int mid = start + (end - start) / 2;

			if (target < nums[mid])
				end = mid - 1;
			else if (target > nums[mid])
				start = mid + 1;
			else {
				ans = mid;
				if (firstOccurence)
					end = mid - 1;
				else
					start = mid + 1;

				System.out.println("start" + start + "\n" + "end" + end + "\n" + "mid: " + mid);

			}

		}

		return ans;

	}

}
