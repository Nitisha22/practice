package binarySearch;

//largest number smaller than target or equal to target --> Floor

public class FloorProblem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] arr = { 4, 6, 7, 45, 67, 89 };

		int target = 41;

		System.out.println(findFloor(arr, target));

	}

	static int findFloor(int[] arr, int target) {

		int start = 0;
		int end = arr.length - 1;

		if (target < arr[start])
			return -1;

		while (start <= end) {
			int mid = start + (end - start) / 2;

			System.out.println("start:" + start + "\n" + "end: " + end + "\n" + "mid: " + mid);

			if (target > arr[mid])
				start = mid + 1;
			else if (target < arr[mid])
				end = mid - 1;
			else
				return arr[mid];

		}
		return arr[end];

	}

}
