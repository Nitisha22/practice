package maze;

public class MazePrintPath {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		pathDiagonally("", 3, 3);

		boolean[][] maze = { { true, false, true }, { true, false, true }, { true, true, true } };
		path("", 3, 3);
		pathRestrictions("", maze, 0, 0);
	}

	public static void path(String p, int r, int c) {

		if (r == 1 && c == 1) {
			System.out.println(p);
			return;
		}

		if (r > 1)
			path(p + 'D', r - 1, c);

		if (c > 1)
			path(p + 'R', r, c - 1);

	}

	public static void pathDiagonally(String p, int r, int c) {

		if (r == 1 && c == 1) {
			System.out.println(p);
			return;
		}

		if (r > 1 && c > 1)
			pathDiagonally(p + 'D', r - 1, c - 1);

		if (r > 1)
			pathDiagonally(p + 'V', r - 1, c);

		if (c > 1)
			pathDiagonally(p + 'H', r, c - 1);

	}

	public static void pathRestrictions(String p, boolean[][] maze, int r, int c) {

		if (r == maze.length - 1 && c == maze[0].length - 1) {
			System.out.println(p);
			return;
		}

		if (!maze[r][c])
			return;

		if (r < maze.length - 1)
			pathRestrictions(p + 'D', maze, r + 1, c);

		if (c < maze[0].length - 1)
			pathRestrictions(p + 'R', maze, r, c + 1);

	}

}
