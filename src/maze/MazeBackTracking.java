package maze;

public class MazeBackTracking {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean[][] maze = { 
				{ true, true, true },
				{ true, true, true }, 
				{ false, true, true }
			};
		pathBackTracking("", maze, 0, 0);
	}

	public static void pathBackTracking(String p, boolean[][] maze, int r, int c) {

		if (r == maze.length - 1 && c == maze[0].length - 1) {
			System.out.println(p);
			return;
		}

		if (!maze[r][c])
			return;

		maze[r][c] = false;

		if (r < maze.length - 1)
			pathBackTracking(p + 'D', maze, r + 1, c);

		if (c < maze[0].length - 1)
			pathBackTracking(p + 'R', maze, r, c + 1);

		if (r > 0)
			pathBackTracking(p + 'U', maze, r - 1, c);

		if (c > 0)
			pathBackTracking(p + 'L', maze, r, c - 1);

		maze[r][c] = true;

	}

}
