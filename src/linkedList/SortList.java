package linkedList;

//https://leetcode.com/problems/sort-list/
public class SortList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

//	  Definition for singly-linked list. 

	public class ListNode {
		int val;
		ListNode next;

		ListNode() {
		}

		ListNode(int val) {
			this.val = val;
		}

		ListNode(int val, ListNode next) {
			this.val = val;
			this.next = next;
		}
	}

	class Solution {
		public ListNode sortList(ListNode head) {

			if (head == null || head.next == null)
				return head;

			ListNode mid = getMid(head);
			ListNode left = sortList(head);
			ListNode right = sortList(mid);

			return merge(left, right);

		}

		// Get Middle Node
		ListNode getMid(ListNode head) {
			ListNode midPrev = null;
			while (head != null && head.next != null) {
				midPrev = (midPrev == null) ? head : midPrev.next;
				head = head.next.next;
			}
			ListNode mid = midPrev.next;
			midPrev.next = null;
			return mid;
		}

		// Merge List
		public ListNode merge(ListNode list1, ListNode list2) {

			if (list1 == null && list2 == null)
				return null;

			if (list1 == null)
				return list2;

			if (list2 == null)
				return list1;

			ListNode temp = new ListNode();
			ListNode m = temp;

			while (list1 != null && list2 != null) {

				if (list1.val < list2.val) {
					m.next = list1;
					list1 = list1.next;
				} else {
					m.next = list2;
					list2 = list2.next;
				}

				m = m.next;

			}

			m.next = list1 != null ? list1 : list2;

			return temp.next;

		}

	}

}
