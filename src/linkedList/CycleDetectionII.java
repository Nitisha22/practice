package linkedList;

//https://leetcode.com/problems/linked-list-cycle-ii/
public class CycleDetectionII {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	class ListNode {
		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
			next = null;
		}
	}

	public class Solution {
		public ListNode detectCycle(ListNode head) {

			ListNode slow = head;
			ListNode fast = head;

			int length = 0;

			while (fast != null && fast.next != null) {

				slow = slow.next;
				fast = fast.next.next;

				if (slow == fast) {
					length = lengthOfCycle(slow);
					break;
				}
			}

			if (length == 0)
				return null;

			ListNode f = head;
			ListNode s = head;

			while (length > 0) {
				s = s.next;
				length--;
			}

			while (f != s) {
				s = s.next;
				f = f.next;
			}

			return s;
		}

		public int lengthOfCycle(ListNode head) {

			ListNode slow = head;
			ListNode fast = head;

			while (fast != null || fast.next != null) {

				slow = slow.next;
				fast = fast.next.next;

				if (slow == fast) {
					ListNode temp = slow;
					int length = 0;
					do {
						temp = temp.next;
						length++;
					} while (temp != slow);

					return length;
				}
			}
			return 0;
		}
	}
}
