package linkedList;

import linkedList.ReverseSLL.ListNode;

public class CycleDetection {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public class ListNode {
		int val;
		ListNode next;

		ListNode() {
		}

		ListNode(int val) {
			this.val = val;
		}

		ListNode(int val, ListNode next) {
			this.val = val;
			this.next = next;
		}
	}

	public static boolean cycle(ListNode head) {

		ListNode slow = head;
		ListNode fast = head.next;

		while (slow != fast) {

			if (fast == null || fast.next == null)
				return false;

			slow = slow.next;
			fast = fast.next.next;
		}

		return true;

	}

	public static int cycleLength(ListNode head) {

		ListNode slow = head;
		ListNode fast = head;
		int length = 0;

		while (fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;

			if (slow == fast) {
				ListNode temp = slow;

				do {
					temp = temp.next;
					length++;
				} while (slow != temp);
			}
		}

		return 0;

	}

}
