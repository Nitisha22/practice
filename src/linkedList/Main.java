package linkedList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		SLL singlyLinkedList = new SLL();
		singlyLinkedList.insertFirst(0);
		singlyLinkedList.insertFirst(1);
		singlyLinkedList.insertFirst(5);
		singlyLinkedList.insertLast(99);
		singlyLinkedList.insert(100, 3);
		singlyLinkedList.display();
//		singlyLinkedList.deleteFirst();
//		singlyLinkedList.display();
//		System.out.println(singlyLinkedList.deleteLast());
//		singlyLinkedList.display();
//		System.out.println(singlyLinkedList.delete(2));
//		singlyLinkedList.display();
//		System.out.println(singlyLinkedList.find(1));

//		DLL doublyLinkedList = new DLL();
//		doublyLinkedList.insertFirst(1);
//		doublyLinkedList.insertFirst(2);
//		doublyLinkedList.insertFirst(3);
//		doublyLinkedList.insertFirst(4);
//		doublyLinkedList.insertLast(99);
//		doublyLinkedList.insert(4, 5);
//		doublyLinkedList.display();

//		CLL circularLinkedList = new CLL();
//		circularLinkedList.insert(1);
//		circularLinkedList.insert(2);
//		circularLinkedList.insert(3);
//		circularLinkedList.insert(4);
//		circularLinkedList.insert(5);
//		circularLinkedList.insert(6);
//		circularLinkedList.display();
//		circularLinkedList.delete(5);
//		circularLinkedList.display();
//
//		SLL singlyLinkedList = new SLL();
		singlyLinkedList.insertRecur(10, 0);
		singlyLinkedList.insertRecur(20, 1);
		singlyLinkedList.display();


	}

}
