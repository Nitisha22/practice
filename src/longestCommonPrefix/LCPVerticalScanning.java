package longestCommonPrefix;

//https://leetcode.com/problems/longest-common-prefix
public class LCPVerticalScanning {

	public static void main(String args[]) {

		String[] strs = new String[] { "flower", "flow", "flight" };
		System.out.println(longestCommonPrefix(strs));
	}

	public static String longestCommonPrefix(String[] strs) {

		String ans = "";

		if (strs == null || strs.length == 0) {
			return ans;
		}

		// min length among all strings
		int minLen = 200;

		for (String str : strs) {
			minLen = Math.min(minLen, str.length());
		}

		// Here first string is being picked up and char of its are being checked with
		// its consecutive strings
		for (int i = 0; i < minLen; i++) {

			char ch = strs[0].charAt(i);

			for (int j = 0; j < strs.length; j++) {

				if (strs[j].charAt(i) != ch)
					return ans;
			}

			ans += ch;
		}

		return ans;

	}

}
