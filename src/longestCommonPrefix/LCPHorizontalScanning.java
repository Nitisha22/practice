package longestCommonPrefix;

public class LCPHorizontalScanning {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String[] strs = new String[] { "flower", "flow", "flight" };
		System.out.println(longestCommonPrefix(strs));

	}

	private static String longestCommonPrefix(String[] strs) {

		if (strs.length == 0 || strs == null)
			return "";

		String prefix = strs[0];

		for (int i = 1; i < strs.length; i++) {

			while (strs[i].indexOf(prefix) != 0) {

				System.out.println("Index Of : " + strs[i].indexOf(prefix));
				prefix = prefix.substring(0, prefix.length() - 1);
				System.out.println("Prefix : " + prefix);
			}

		}

		// TODO Auto-generated method stub
		return prefix;
	}

}
