package sorting;

import java.util.Arrays;

public class MergeSortInPlace {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = { 5, 4, 3, 2, 1 };
		mergeSortInPlace(arr, 0, arr.length);
		System.out.println(Arrays.toString(arr));

	}

	public static void mergeSortInPlace(int[] arr, int s, int e) {

		if (e - s == 1)
			return;

		int mid = (s + e) / 2;

		System.out.println("Start : " + s);

		System.out.println("Mid : " + mid);

		System.out.println("End : " + e);

		mergeSortInPlace(arr, s, mid);

		mergeSortInPlace(arr, mid, e);

		mergeInPlace(arr, s, mid, e);

	}

	private static void mergeInPlace(int[] arr, int s, int m, int e) {
		// TODO Auto-generated method stub

		int i = s;
		int j = m;
		int k = 0;

		int[] res = new int[e - s];

		while (i < m && j < e) {
			if (arr[i] < arr[j]) {

				res[k] = arr[i];
				i++;
			} else {
				res[k] = arr[j];
				j++;
			}
			k++;
		}

		while (i < m) {
			res[k] = arr[i];
			i++;
			k++;
		}

		while (j < e) {
			res[k] = arr[j];
			j++;
			k++;
		}
		for (int l = 0; l < res.length; l++)
			arr[s + l] = res[l];

	}
}
