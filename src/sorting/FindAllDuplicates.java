package sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindAllDuplicates {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nums = { 1, 2, 4, 3, 4, 3, 4, 5, 5, 6, 1 };
		System.out.println(findDuplicates(nums));
	}

	public static List<Integer> findDuplicates(int[] nums) {
		int i = 0;
		while (i < nums.length) {
			int correctIndex = nums[i] - 1;
			if (nums[i] != nums[correctIndex])
				swap(nums, i, correctIndex);
			else
				i++;
		}

		System.out.println(Arrays.toString(nums));

		List<Integer> ans = new ArrayList<>();

		for (int index = 0; index < nums.length; index++) {
			if (nums[index] != index + 1)
				ans.add(nums[index]);

		}

		return ans;

	}

	public static int[] swap(int nums[], int first, int second) {
		int temp = nums[first];
		nums[first] = nums[second];
		nums[second] = temp;

		return nums;
	}
}
