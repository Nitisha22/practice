package sorting;

import java.util.Arrays;

public class MSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = { 5, 4, 3, 2, 1, 0, 7, 9, 8 };
		mergeSort(arr, 0, arr.length - 1);
		System.out.println(Arrays.toString(arr));
	}

	public static void mergeSort(int[] arr, int l, int r) {

		if (l < r) {

			int mid = (l + r) / 2;

			mergeSort(arr, l, mid);
			mergeSort(arr, mid + 1, r);
			merge(arr, l, mid, r);
		}
	}

	public static void merge(int[] arr, int l, int m, int r) {

		int i = l;
		int j = m + 1;
		int k = l;
		int res[] = new int[arr.length];

		while (i <= m && j <= r) {

			if (arr[i] < arr[j]) {
				res[k] = arr[i];
				i++;
			} else {
				res[k] = arr[j];
				j++;
			}
			k++;
		}

		while (i <= m) {
			res[k] = arr[i];
			i++;
			k++;
		}

		while (j <= r) {
			res[k] = arr[j];
			j++;
			k++;
		}

		for (k = l; k <= r; k++)
			arr[k] = res[k];
	}

}
