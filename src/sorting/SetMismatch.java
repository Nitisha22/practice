package sorting;

import java.util.Arrays;

//https://leetcode.com/problems/set-mismatch/
public class SetMismatch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nums = { 1, 2, 2, 4 };
		System.out.println(Arrays.toString(findErrorNums(nums)));
	}

	public static int[] findErrorNums(int[] nums) {
		int i = 0;
		while (i < nums.length) {
			int correctIndex = nums[i] - 1;
			if (nums[i] != nums[correctIndex]) {
				swap(nums, i, correctIndex);
				System.out.println(Arrays.toString(nums));
			} else
				i++;
		}
		System.out.println(Arrays.toString(nums));

		for (int index = 0; index < nums.length; index++) {
			if (nums[index] != index + 1)
				return new int[] { nums[index], index + 1 };
		}
		return new int[] { -1, -1 };
	}

	// Swapping the max and last pointing index
	public static int[] swap(int[] arr, int originalIndex, int correctIndex) {

		int temp = arr[originalIndex];
		arr[originalIndex] = arr[correctIndex];
		arr[correctIndex] = temp;

		return arr;

	}
}
