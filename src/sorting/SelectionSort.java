package sorting;

import java.util.Arrays;

public class SelectionSort {

	public static void main(String args[]) {

		int[] arr = { 4, 5, 6, 1, 2, 3 };
		System.out.println(Arrays.toString(selectionSort(arr)));
	}

	// it will find the max element, swap with the (endElement - i),
	// so by this every element from the last gets sorted means it will pick the max
	// element and put at the last
	// then second max element at second last and so on.
	public static int[] selectionSort(int[] arr) {

		for (int i = 0; i < arr.length; i++) {

			int last = arr.length - i - 1;
			int maxIndex = getMaxIndexElement(arr, 0, last);
			swap(arr, maxIndex, last);
		}

		return arr;
	}

	// get element for max index
	public static int getMaxIndexElement(int[] arr, int start, int last) {

		int max = start;

		for (int i = start; i <= last; i++) {

			if (arr[max] < arr[i])
				max = i;
		}

		return max;

	}

	// Swapping the max and last pointing index
	public static int[] swap(int[] arr, int maxIndex, int last) {

		int temp = arr[maxIndex];
		arr[maxIndex] = arr[last];
		arr[last] = temp;

		return arr;

	}

}
