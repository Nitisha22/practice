package sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/
public class FindNumDisappeared {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = { 4, 3, 2, 7, 8, 2, 3, 1 };
		System.out.println(missingNumber(arr));
	}

	public static List<Integer> missingNumber(int[] nums) {
		int i = 0;
		while (i < nums.length) {
			int correctIndex = nums[i] - 1;
			if (nums[i] != nums[correctIndex])
				swap(nums, i, correctIndex);
			else
				i++;
		}

		System.out.println(Arrays.toString(nums));
		List<Integer> ans = new ArrayList<>();
		for (int index = 0; index < nums.length; index++) {
			if (nums[index] != index + 1)
				ans.add(index + 1);
		}
		return ans;
	}

	// Swapping the max and last pointing index
	public static int[] swap(int[] arr, int originalIndex, int correctIndex) {

		int temp = arr[originalIndex];
		arr[originalIndex] = arr[correctIndex];
		arr[correctIndex] = temp;

		return arr;

	}

}
