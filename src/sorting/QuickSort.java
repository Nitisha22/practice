package sorting;

import java.util.Arrays;
import java.util.Random;

public class QuickSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = { 4, 5, 6, 1, 3, 8, 2 };

		quickSort(arr, 0, arr.length - 1);
		System.out.println(Arrays.toString(arr));
	}

	public static void quickSort(int[] arr, int l, int h) {
		if (l < h) {
			int pivotElement = partition(arr, l, h);
			quickSort(arr, l, pivotElement - 1);
			quickSort(arr, pivotElement + 1, h);
		}

	}

	public static int partition(int[] arr, int l, int h) {

		int mid = (l+ h)/2;
		int pivot = arr[l];
		int i = l;
		int j = h;

		while (i < j) {

			while (arr[i] <= pivot)
				i++;

			while (arr[j] > pivot)
				j--;

			if (i < j)
				swap(arr, i, j);

		}
		swap(arr, j, l);

		return j;
	}

	private static void swap(int[] arr, int first, int second) {
		// TODO Auto-generated method stub
		int temp = arr[first];
		arr[first] = arr[second];
		arr[second] = temp;

	}

}
