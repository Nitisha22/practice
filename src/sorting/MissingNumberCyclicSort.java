package sorting;

import java.util.Arrays;

//https://leetcode.com/problems/missing-number/submissions/
public class MissingNumberCyclicSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = { 0, 2, 4, 1 };
		System.out.println(missingNumber(arr));
	}

	public static int missingNumber(int[] nums) {
		int i = 0;
		while (i < nums.length) {
			int correctIndex = nums[i];
			if (nums[i] < nums.length && nums[i] != nums[correctIndex]) {
				swap(nums, i, correctIndex);
				System.out.println(Arrays.toString(nums));
			} else
				i++;
		}
		System.out.println(Arrays.toString(nums));

		for (int index = 0; index < nums.length; index++) {
			if (nums[index] != index)
				return index;
		}
		return nums.length;
	}

	// Swapping the max and last pointing index
	public static int[] swap(int[] arr, int originalIndex, int correctIndex) {

		int temp = arr[originalIndex];
		arr[originalIndex] = arr[correctIndex];
		arr[correctIndex] = temp;

		return arr;

	}

}
