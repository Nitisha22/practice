package sorting;

import java.util.Arrays;

public class CyclicSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = { 3, 2, 1, 4, 5 };
		System.out.println(Arrays.toString(cyclicSort(arr)));

	}

	public static int[] cyclicSort(int[] arr) {

		int i = 0;
		while (i < arr.length) {

			int correctIndex = arr[i] - 1;
			if (arr[i] != arr[correctIndex])
				swap(arr, i, correctIndex);
			else
				i++;
		}
		return arr;

	}

	// Swapping the max and last pointing index
	public static int[] swap(int[] arr, int originalIndex, int correctIndex) {

		int temp = arr[originalIndex];
		arr[originalIndex] = arr[correctIndex];
		arr[correctIndex] = temp;

		return arr;

	}
}
