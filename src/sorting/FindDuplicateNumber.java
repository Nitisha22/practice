package sorting;

import java.util.Arrays;

//https://leetcode.com/problems/find-the-duplicate-number
public class FindDuplicateNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nums = { 1, 2, 4, 3, 4 };
		System.out.println(findDuplicate(nums));
		System.out.println(findDuplicateSecondMethod(nums));

	}

	public static int findDuplicate(int[] nums) {

		int i = 0;
		while (i < nums.length) {
			if (nums[i] != i + 1) {
				int correctIndex = nums[i] - 1;
				if (nums[i] != nums[correctIndex])
					swap(nums, i, correctIndex);
				else
					return nums[i];
			} else
				i++;

		}
		return -1;
	}

	public static int findDuplicateSecondMethod(int[] nums) {

		int i = 0;
		while (i < nums.length) {
			int correctIndex = nums[i] - 1;
			if (nums[i] != nums[correctIndex])
				swap(nums, i, correctIndex);
			else
				i++;

		}

		for (int index = 0; index < nums.length; index++) {
			if (nums[index] != index + 1)
				return nums[index];
		}
		return -1;

	}

	public static int[] swap(int nums[], int first, int second) {
		int temp = nums[first];
		nums[first] = nums[second];
		nums[second] = temp;

		return nums;
	}

}
