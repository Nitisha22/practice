package sorting;

import java.util.Arrays;

//https://leetcode.com/problems/first-missing-positive/
public class FirstMissingPositive {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nums = { -1, 0, 1, 2, 3, 6, 4, 5 };
		System.out.println(firstMissingPositive(nums));

	}

	public static int firstMissingPositive(int[] nums) {

		int i = 0;

		while (i < nums.length) {
			int correctIndex = nums[i] - 1;
			if (nums[i] > 0 && nums[i] < nums.length && nums[i] != nums[correctIndex]) {
				swap(nums, i, correctIndex);
			} else
				i++;
		}

		System.out.println(Arrays.toString(nums));
		for (int index = 0; index < nums.length; index++) {
			if (nums[index] != index + 1)
				return index + 1;

		}
		System.out.println(nums.length);

		return nums.length + 1;
	}

	public static int[] swap(int[] nums, int first, int second) {
		int temp = nums[first];
		nums[first] = nums[second];
		nums[second] = temp;

		return nums;
	}
}
