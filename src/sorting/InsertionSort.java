package sorting;

import java.util.Arrays;

public class InsertionSort {

	public static void main(String args[]) {

		int[] arr = { 1, 2, 3, 4, 5, 3 };
		System.out.println(Arrays.toString(insertionSort(arr)));
	}

	public static int[] insertionSort(int[] arr) {

		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = i + 1; j > 0; j--) {
				if (arr[j - 1] > arr[j])
					swap(arr, j - 1, j);
				else
					break;
			}
		}

		return arr;

	}

	public static int[] swap(int[] arr, int first, int second) {
		int temp = arr[first];
		arr[first] = arr[second];
		arr[second] = temp;

		return arr;
	}
}
