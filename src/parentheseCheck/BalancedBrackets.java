package parentheseCheck;

import java.util.Stack;

public class BalancedBrackets {

	public static void main(String args[]) {

		String bracket1 = "{{[()]}}";
		String bracket2 = "()()";

		System.out.println(isBalanced(bracket1));
		System.out.println(isBalanced(bracket2));
	}

//	Four conditions:
//		1. If opening bracket then push the character on stack and continue
//		2. If closing bracket the pop the element and peek if the element is correct or not
//		3. If stack is empty, means if there is closing bracket but no element on stack then return false
//		4. If stack is not empty after the completion of brackets then, return false
//	    Complexity is O(n)
	
	public static boolean isBalanced(String str) {

		boolean flag = true;

		Stack<Character> stack = new Stack<>();

		for (int i = 0; i < str.length(); i++) {

			if (str.charAt(i) == '(' || str.charAt(i) == '{' || str.charAt(i) == '[') {
				stack.push(str.charAt(i));
				System.out.println(str.charAt(i));
				continue;
			}

			if (stack.isEmpty()) {
				return false;
			}

			char ch = str.charAt(i);
			switch (ch) {
			case ')':
				if (stack.peek() == '(')
					stack.pop();
				else
					flag = false;
				break;
			case ']':
				if (stack.peek() == '[')
					stack.pop();
				else
					flag = false;
				break;
			case '}':
				if (stack.peek() == '{')
					stack.pop();
				else
					flag = false;
				break;
			}

		}

		if (!stack.isEmpty()) {
			flag = false;
		}

		return flag;

	}

}
