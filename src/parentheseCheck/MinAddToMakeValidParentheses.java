package parentheseCheck;

import java.util.Stack;

//https://leetcode.com/problems/minimum-add-to-make-parentheses-valid/
public class MinAddToMakeValidParentheses {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String str = "())";
		
		System.out.println(minAddToMakeValid(str));

	}

	public static int minAddToMakeValid(String s) {

		Stack<Character> stack = new Stack<>();

		for (char ch : s.toCharArray()) {

			if (ch == '(') {
				stack.push(ch);
			} else {
				if (!stack.isEmpty() && stack.peek() == '(') {
					stack.pop();
				} else
					stack.push(ch);
			}

		}
		return stack.size();
	}
}
