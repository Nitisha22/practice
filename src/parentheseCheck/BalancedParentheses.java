package parentheseCheck;

public class BalancedParentheses {

//	Given a string str of length N, consisting of �(� and �)� only,
//	the task is to check whether it is balanced or not.

	public static void main(String args[]) {

		String check1 = "((()))()";
		String check2 = "(()))(";

		System.out.println("--------------Check 1 -----------\n" + isBalanced(check1));
		System.out.println("--------------Check 2 -----------\n" + isBalanced(check2));

	}
	
//	Four Conditions:
//	Count ++ for opening bracket
//	Count -- for closing bracket
//	If Count is in -ve then break from loop and return false
//	If Count is not equal to 0 then return false

	public static boolean isBalanced(String str) {

		boolean flag = true;
		int count = 0;

		for (int i = 0; i < str.length(); i++) {

			char ch = str.charAt(i);
			if (ch == '(') {
				count++;
			} else {
				count--;
			}

			if (count < 0) {
				flag = false;
				break;
			}
		}

		if (count != 0) {
			flag = false;
		}

		return flag;

	}
}
