package RemoveDuplicatesFromSortedArray;

public class RemoveDuplicates {

	public static void main(String args[]) {

		int arr[] = { 1, 1, 2, 3, 3, 3, 3, 4 };
		int length = arr.length;
		arr = removeDuplicates(arr, length);

		for (int array : arr) {
			System.out.println(array);
		}

		System.out.println("Length of Array : " + arr.length);

	}

	public static int[] removeDuplicates(int arr[], int n) {

		{
			if (n == 0 || n == 1)
				return arr;

			// To store index of next unique element
			int j = 0;

			int temp[] = new int[n];

			// Doing same as done in Method 1
			// Just maintaining another updated index i.e. j
			for (int i = 0; i < n - 1; i++)
				if (arr[i] != arr[i + 1])
					temp[j++] = arr[i];

			temp[j++] = arr[n - 1];

			return temp;
		}

	}
}
