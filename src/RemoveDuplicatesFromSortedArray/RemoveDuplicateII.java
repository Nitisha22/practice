package RemoveDuplicatesFromSortedArray;

import java.util.HashSet;

public class RemoveDuplicateII {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int arr[] = { 1, 1, 2, 3, 3, 3, 3, 4 };
		System.out.println(removeDuplicates(arr));

	}

	public static int removeDuplicates(int[] nums) {

		HashSet<Integer> hashset = new HashSet<Integer>();

		for (int num : nums) {

			if (hashset.contains(num)) {
				continue;
			}
			hashset.add(num);
		}

		System.out.println(hashset);
		
		return hashset.size();

	}

}
