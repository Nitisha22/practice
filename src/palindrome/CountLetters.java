package palindrome;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CountLetters {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Scanner sc = new Scanner(System.in);
	String str = sc.nextLine();
		
	checkCharacterCount(str);

	}
	
	public static void checkCharacterCount(String str){

		if(str == null)
			return;		
		
		char[] c = str.toCharArray();
		Map<Character, Integer> map = new HashMap();
		
		for(int i = 0 ; i < c.length; i++){
		
			if(map.containsKey(c[i])){
				map.put( c[i] , map.get(c[i]) + 1);
			}else
			
			map.put(c[i] , 1);
		
		}


		System.out.println("Map:");
		System.out.println(map);

	}

}
