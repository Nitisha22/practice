package palindrome;

public class NumberPalindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x = 939;
		System.out.println(isPalindrome(x));
		System.out.println(isPalindrome(648));

	}

	public static boolean isPalindrome(int x) {

		int reverse = 0;
		int remainder = 0;
		int num = x;

		while (x > 0) {
			remainder = x % 10;
			reverse = reverse * 10 + remainder;
			x = x / 10;
		}
		System.out.println("Reverse Number : "+reverse);

		if (num == reverse)
			return true;

		return false;
	}

}
