package palindrome;

public class PalindromeOfNum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x = 939;
		System.out.println(isPalindrome(x));
	}

	public static boolean isPalindrome(int x) {

		String originalValue = String.valueOf(x);

		StringBuilder reverseValue = new StringBuilder();
		reverseValue.append(originalValue);
		reverseValue.reverse();

		if (originalValue.equals(reverseValue.toString()))
			return true;

		return false;
	}

}
