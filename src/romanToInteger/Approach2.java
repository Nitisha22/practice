package romanToInteger;

public class Approach2 {

	public static void main(String args[]) {

		String romanValue = "MCMIV";
		System.out.println(romanToInteger(romanValue));
	}

	public static int value(Character ch) {

		if (ch == 'M')
			return 1000;
		if (ch == 'D')
			return 500;
		if (ch == 'C')
			return 100;
		if (ch == 'L')
			return 50;
		if (ch == 'X')
			return 10;
		if (ch == 'I')
			return 1;
		if (ch == 'V')
			return 5;

		return -1;
	}

	public static int romanToInteger(String str) {

		int sum = 0;
		int len = str.length();

		for (int i = 0; i < len; i++) {

			if (i != len - 1 && value(str.charAt(i)) < value(str.charAt(i + 1))) {
				sum += value(str.charAt(i + 1)) - value(str.charAt(i));
				i++;
			} else {
				sum += value(str.charAt(i));
			}
		}
		return sum;

	}

}
