package romanToInteger;

import java.util.HashMap;
import java.util.Map;

public class RomanToInteger {

	private static final Map<Character, Integer> roman = new HashMap<Character, Integer>() {
		{
			put('I', 1);
			put('V', 5);
			put('X', 10);
			put('L', 50);
			put('C', 100);
			put('D', 500);
			put('M', 1000);
		}
	};

	public static int getIntegerValue(String str) {

		int sum = 0;
		int len = str.length();

		for (int i = 0; i < len; i++) {

			if (i != len - 1 && roman.get(str.charAt(i)) < roman.get(str.charAt(i + 1))) {
				sum += roman.get(str.charAt(i + 1)) - roman.get(str.charAt(i));
				System.out.println(sum);
				i++;

			} else {
				sum += roman.get(str.charAt(i));
				System.out.println(sum);

			}
		}
		return sum;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String romanValue = "MCMIV";

		System.out.println(getIntegerValue(romanValue));

	}

}
